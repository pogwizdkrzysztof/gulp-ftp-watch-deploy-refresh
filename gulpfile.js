'use strict';

var gulp = require('gulp');
var gutil = require( 'gulp-util' );
var ftp = require( 'vinyl-ftp' );
var through = require('through2');
var browserSync = require('browser-sync').create();

/** Configuration **/
var user = "";
var password = "";
var host = '';
var port = 21;
var localFilesGlob = ['./css/**/*'];
var remoteFolder = '/public_html';

// helper function to build an FTP connection based on our configuration
function getFtpConnection() {
	return ftp.create({
		host: host,
		port: port,
		user: user,
		password: password,
		parallel: 5,
		log: gutil.log,
		secure: true,
		secureOptions: {rejectUnauthorized: false}
	});
}

/**
 * Deploy task.
 * Copies the new files to the server
 *
 * Usage: `FTP_USER=someuser FTP_PWD=somepwd gulp ftp-deploy`
 */
gulp.task('ftp-deploy', function() {

	var conn = getFtpConnection();

	return gulp.src(localFilesGlob, { base: '.', buffer: false })
		.pipe( conn.newer( remoteFolder ) ) // only upload newer files
		.pipe( conn.dest( remoteFolder ) )
		;
});

/**
 * Watch deploy task.
 * Watches the local copy for changes and copies the new files to the server whenever an update is detected
 *
 * Usage: `FTP_USER=someuser FTP_PWD=somepwd gulp ftp-deploy-watch`
 */
gulp.task('ftp-deploy-watch', function() {

	var conn = getFtpConnection();

	browserSync.init({
		proxy: "pogwizd.pl"
	});

	gulp.watch(localFilesGlob, function (vinyl) {

		console.log('Changes detected! Uploading file "' + vinyl.path + '"');

		return gulp.src( [vinyl.path], { base: '.', buffer: false } )
			.pipe( conn.newer( remoteFolder ) ) // only upload newer files
			.pipe( conn.dest( remoteFolder ) )
			.pipe( through.obj( reloadBrowser ) )
			;
	});


});


function reloadBrowser (chunk, enc, cb) {
	browserSync.reload();
	cb(null, chunk)
}